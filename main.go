// Navdump converts the MuST navigation data to ndJSON
package main

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaploe/navsvc/pkg/atlas"
	"bitbucket.org/uwaploe/navsvc/pkg/boss"
)

const Usage = `Usage: navdump [options] infile

`

var Version = "dev"
var BuildDate = "unknown"

type myTime time.Time

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	outFile       string
	tFrom, tUntil myTime
)

func (t *myTime) String() string {
	return fmt.Sprint(*t)
}

// Parse_datetime is a flexible ISO8601-ish date/time parser. It handles
// the following formats (missing least-significant values are assumed
// to be zero):
//
//    YYYY-mm-ddTHH:MM:SS-ZZZZ
//    YYYY-mm-ddTHH:MM:SS   (UTC assumed)
//    YYYY-mm-ddTHH:MM
//    YYYY-mm-dd
//
func (mt *myTime) Set(dt string) error {
	t, err := time.Parse("2006-01-02T15:04:05-0700", dt)
	if err != nil {
		t, err = time.Parse("2006-01-02T15:04:05", dt)
		if err != nil {
			t, err = time.Parse("2006-01-02T15:04", dt)
			if err != nil {
				t, err = time.Parse("2006-01-02", dt)
			}
		}
	}
	*mt = myTime(t)
	return err
}

func reader(rdr io.Reader) <-chan atlas.MustNavMessage {
	ch := make(chan atlas.MustNavMessage, 1)
	pad := binary.Size(boss.JsfEdpPadding{})
	go func() {
		defer close(ch)
		scanner := boss.NewScanner(rdr)
		for scanner.Scan() {
			data := scanner.Payload()
			rec := atlas.MustNavMessage{}
			err := binary.Read(bytes.NewReader(data[pad:]), boss.ByteOrder, &rec)
			if err != nil {
				break
			}
			ch <- rec
		}
	}()
	return ch
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&outFile, "out", "", "write output to named file")
	flag.Var(&tFrom, "since", "show data records on or newer than specified date")
	tUntil = myTime(time.Now())
	flag.Var(&tUntil, "until", "show data records on or older than specified date")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	fin, err := os.Open(args[0])
	if err != nil {
		log.Fatalf("Open %q: %v", args[0], err)
	}

	var (
		fout io.WriteCloser
	)

	if outFile != "" {
		fout, err = os.Create(outFile)
		if err != nil {
			log.Fatalf("Open %q: %v", outFile, err)
		}
		defer fout.Close()
	} else {
		fout = os.Stdout
	}

	ch := reader(fin)
	enc := json.NewEncoder(fout)

	for rec := range ch {
		t := rec.T.AsTime()
		if t.After(time.Time(tFrom)) && t.Before(time.Time(tUntil)) {
			enc.Encode(rec)
		}
	}

}
